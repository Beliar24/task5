package com.java.string.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ServiceTest {

    Service service;

    String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot", "cherry",
            "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear",
            "pepper", "pineapple", "pumpkin", "potato"};

    @BeforeEach
    public void setUp() {
        service = Mockito.mock(Service.class);
    }

    @Test
    void shouldBeCorrectWord() {
        Mockito.when(service.gameStart(service.getHiddenWord(words))).thenReturn("pear");

        assertEquals(service.gameStart(service.getHiddenWord(words)), "pear");
    }

    @Test
    void shouldBeCorrectOutput() {
        Mockito.when(service.gameStart(service.getHiddenWord(words))).thenReturn("potato");

        assertNotEquals(service.gameStart(service.getHiddenWord(words)), "pear");
    }

}