package com.java.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringTrainTest {

    StringTrain stringTrain = new StringTrain();

    @Test
    void shouldBeCorrectCount() {
        assertEquals(2, stringTrain.findSymbolOccurrence("hello", 'l'));
        assertEquals(1, stringTrain.findSymbolOccurrence("apple", 'a'));
    }

    @Test
    void shouldBeIncorrectCount() {
        assertNotEquals(2, stringTrain.findSymbolOccurrence("car", 'r'));
        assertNotEquals(1, stringTrain.findSymbolOccurrence("rock", 'a'));
    }

    @Test
    void shouldBeIndexOfStartSubstring() {
        assertEquals(3, stringTrain.findWordPosition("Camaro", "aro"));
        assertEquals(1, stringTrain.findWordPosition("Horse", "or"));
    }

    @Test
    void shouldNotBeIndexOfStartSubstring() {
        assertEquals(-1, stringTrain.findWordPosition("Camaro", "to"));
        assertEquals(-1, stringTrain.findWordPosition("Horse", "sa"));
    }

    @Test
    void shouldReversString() {
        assertEquals("egnarO", stringTrain.stringReverse("Orange"));
        assertEquals("oTaToP", stringTrain.stringReverse("PoTaTo"));
    }

    @Test
    void shouldIsPalindrome() {
        assertTrue(stringTrain.isPalindrome("Elle"));
        assertFalse(stringTrain.isPalindrome("Robot"));
    }

}