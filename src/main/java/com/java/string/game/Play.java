package com.java.string.game;

import java.util.Scanner;

public class Play {

    public static void main(String[] args) {

        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot", "cherry",
                "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear",
                "pepper", "pineapple", "pumpkin", "potato"};


        Service service = new Service(new Scanner(System.in));

        System.out.println(service.gameStart(service.getHiddenWord(words)));
    }
}
