package com.java.string.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Service {

    private final List<Character> listHidden = new ArrayList<>();
    private final List<Character> listPlayer = new ArrayList<>();
    private final Scanner scanner;

    public Service(Scanner scanner) {
        this.scanner = scanner;
    }

    public String getHiddenWord(String[] words) {
        Random random = new Random();

        String hiddenWord = words[random.nextInt(words.length + 1)];

        for (int i = 0; i < hiddenWord.length(); i++) {
            listHidden.add(hiddenWord.charAt(i));
        }
        return hiddenWord;
    }

    public String gameStart(String hiddenWord) {

        while (true) {
            StringBuilder st = new StringBuilder();
            String playerWord = scanner.nextLine();

            for (int i = 0; i < playerWord.length(); i++) {
                listPlayer.add(playerWord.charAt(i));
            }

            int length = Math.min(hiddenWord.length(), playerWord.length());

            for (int i = 0; i < length; i++) {
                if (listHidden.get(i).equals(listPlayer.get(i))) {
                    st.append(listHidden.get(i));
                } else {
                    break;
                }
            }

            if (listHidden.equals(listPlayer)) {
                return st.toString();
            }

            st.append("###############");
            System.out.println(st);

            listPlayer.clear();
        }
    }
}
