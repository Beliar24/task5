package com.java.string;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringTrain {

    public int findSymbolOccurrence(String str, char ch) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ch) {
                count++;
            }
        }
        return count;
    }

    public int findWordPosition(String source, String target) {
        return source.indexOf(target);
    }

    public String stringReverse(String str) {
        List<Character> list = new ArrayList<>();

        for (int i = 0; i < str.length(); i++) {
            list.add(str.charAt(i));
        }

        Collections.reverse(list);
        StringBuilder st = new StringBuilder();

        for (char ch : list) {
            st.append(ch);
        }

        return st.toString();
    }

    public boolean isPalindrome(String str) {
        List<Character> list = new ArrayList<>();

        for (int i = 0; i < str.length(); i++) {
            list.add(str.charAt(i));
        }

        Collections.reverse(list);

        StringBuilder reverseString = new StringBuilder();

        for (char ch : list) {
            reverseString.append(ch);
        }

        return reverseString.toString().toLowerCase().equals(str.toLowerCase());
    }
}
